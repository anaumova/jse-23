package ru.tsc.anaumova.tm.exception.field;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class ExistsLoginException extends AbstractException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}