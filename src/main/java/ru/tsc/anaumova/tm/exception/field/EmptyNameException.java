package ru.tsc.anaumova.tm.exception.field;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}